<?php

class Waybill {
    private $cargoItems = [];
    private $cargoItemWeights = [];
    private $totalWeight = 0;

    function __construct(){
        $this->cargoItems = [];
        $this->cargoItemWeights = [];
        $this->totalWeight = 0;
    }

    function addCargoItem(iCargoItem $item){
        $itemId = $item->getId();
        $this->cargoItems[$itemId] = $item;
        $this->cargoItemWeights[$itemId] = $item->getWeight();
        $this->totalWeight += $this->cargoItemWeights[$itemId];
    }

    function getCargoItems(): array{
        return $this->cargoItems;
    }

    function getTotalWeight(): int {
        return $this->totalWeight;
    }

    function getCargoItemsWeights(): array{
        return $this->cargoItemWeights;
    }

    function takeOffCargoItem(string $itemId): iCargoItem{
        $item = $this->cargoItems[$itemId];
        //echo "\n\t* I'm taking off element $itemId.\n\t  ".$item;
        $this->totalWeight-=$this->cargoItemWeights[$itemId];
        unset($this->cargoItems[$itemId]);
        unset($this->cargoItemWeights[$itemId]);
        return $item;
    }

    function depart(){
        echo "\n I'm approved and leaving :)";
        self::__construct();
    }

    function evaluateCargoItemsWeights(int $currentOverload): array {
        $evaluations = [];
        foreach($this->cargoItemWeights as $itemId => $itemWeight){
            $evaluations[$itemId] = ($currentOverload-$itemWeight) / $currentOverload;
            arsort($evaluations);
        }
        return $evaluations;
    }

    function __toString(){
        return sprintf("\n I have %d item(s) with %d kg total weight.", count($this->cargoItems), $this->totalWeight);
    }
}