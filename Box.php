<?php

class Box implements iCargoItem{
    var $id;
    var $weight;
    var $postDate;
    function __construct(float $weight = 0, \DateTime $postDate = null){
        $this->weight = $weight ? : rand(10,20);
        $this->postDate = $postDate ? : new \DateTime();
        $rand = rand(0,999);
        $this->id = "B".substr(md5($this->weight.$rand), 0, 4).":$rand";
    }

    function getId(): string {
        return $this->id;
    }

    function getWeight(): float {
        return $this->weight;
    }

    function getPostDate(): \DateTime {
        return $this->postDate;
    }

    function __toString()
    {
        return "Box named $this->id. Weight: $this->weight kg, posted on ".$this->postDate->format('Y-m-d, H:i:s');
    }
}

