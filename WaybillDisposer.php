<?php
class WaybillDisposer{
    private $inWaybill;
    private $maxLoad;
    private $outWaybills;

    function __construct(Waybill $inWayBill, int $maxLoad){
        $this->inWaybill = $inWayBill;
        $this->maxLoad = $maxLoad;
    }

    function dispose(){
        if($this->inWaybill->getTotalWeight() < $this->maxLoad){
            // current waybill is not overloaded
            $this->outWaybills[] = clone $this->inWaybill;
        } else {
            // current waybill is overloaded
            // create a waybill with all items
            $outWayBill = clone $this->inWaybill;
            // while weight of all items on waybill is greater than maximum vehicle's capacity
            while(($overload = $outWayBill->getTotalWeight() - $this->maxLoad) > 0){
                // create new "leftovers" waybill
                $toDeliverWayBill = new Waybill();
                // while waybill is still overoaded...
                while(($overload = $outWayBill->getTotalWeight() - $this->maxLoad)>0){
                    // evaluate cargo items uing their weight
                    $cargoItemsEvaluationIds = array_keys($outWayBill->evaluateCargoItemsWeights($overload));
                    // move first, potencially the best one, to "leftovers"
                    $toDeliverWayBill->addCargoItem($outWayBill->takeOffCargoItem($cargoItemsEvaluationIds[0]));
                }
                // prepared waybill is ready to depart
                $this->outWaybills[] = clone $outWayBill;
                // lets take a look at the "leftovers"
                $outWayBill = clone $toDeliverWayBill;
            }
            // last waybill is ready to depart
            $this->outWaybills[] = clone $outWayBill;
        }
        return $this->outWaybills;
    }
}

