<?php
/*
Kazdego dnia o godzinie 8:00 do hangaru przyjezdza 1 ciezarówka, które moze przewiezc od 5 do 40 pudel towaru (BOX).
Kazde pudlo moze wazyc 10-20kg. Kolejna ciezarówka gabarytowa przewozi 2 maszyny rolnicze (MACHINE), wazace 1.5t oraz 2t.

Firma posiada samochód dostawczy (TRUCK) o maksymalnej wadze zaladunku 200kg, oraz samolot (PLANE), którym przewozi
maszyny rolnicze.

Zadaniem osoby pakujacej jest przepakowanie towaru z ciezarówki do samochodów dostawczych które sa w stanie przewiezc
paczki o lacznej wadze 200kg kazdy. Oraz przepakowanie maszyn rolniczych z ciezarówki gabarytowej na samolot. Wazne, aby
osoba pakujaca miala wiedze która paczka znajduje sie w którym ze srodków transportu.

Na zadanie skladaja sie nastepujace punkty:
1. dostarczenie osobie odbierajacej towaru do rozladunku,
2. pomoc w przeladunku.
*/

const MAX_DAYS = 4;
const MAX_TRUCK_LOAD = 200;
const MAX_PLANE_LOAD = 3600;
const MIN_IN_BOXES = 15;
const MAX_IN_BOXES = 40;
const MIN_IN_MACHINES = 1;
const MAX_IN_MACHINES = 2;

require_once('CargoItem.php');
require_once('Box.php');
require_once('Machine.php');
require_once('Waybill.php');
require_once('WaybillDisposer.php');

for($day=1; $day<=MAX_DAYS; $day++){
    printHeaderBox("Day #$day", 13, "~", "~");
    // the truck arrives with packages
    $inTruckWaybill = new Waybill();
    for($boxNum = rand(MIN_IN_BOXES,MAX_IN_BOXES); $boxNum>0; $boxNum--){
        $inTruckWaybill->addCargoItem(new \Box());
    }

    // the truck arrives with machines
    $inPlaneWaybill = new Waybill();
    for($machineNum = rand(MIN_IN_MACHINES,MAX_IN_MACHINES); $machineNum>0; $machineNum--){
        $inPlaneWaybill->addCargoItem(new \Machine());
    }

    $dailyTransport = [
        'TRUCK' => [
            'in' => $inTruckWaybill,
            'disposition' => (new WaybillDisposer($inTruckWaybill, MAX_TRUCK_LOAD))->dispose()
        ],
        'PLAIN' => [
            'in' => $inPlaneWaybill,
            'disposition' => (new WaybillDisposer($inPlaneWaybill, MAX_PLANE_LOAD))->dispose()
        ],
    ];

    foreach ($dailyTransport as $transportType => $transportItems){
        printHeaderBox("Day #$day: Delivery by $transportType", 4);
        echo "{$transportItems['in']}\n    * ".join("\n    * ", $transportItems['in']->getCargoItems());
        printHeaderBox("Day #$day: Delivery by $transportType - dispatch", 2, ' ', '-');
        foreach($transportItems['disposition'] as $waybill){
            echo "$waybill\n    * ".join("\n    * ", $waybill->getCargoItems());
        }
    }
}

function printHeaderBox(string $content, int $paddingLength = 2, string $topBottom = '-', string $leftRight = '|'){
    $contentLength = mb_strlen($content);
    $lineLength = $contentLength + 2 * $paddingLength + 2;
    $horizontalLine = str_repeat($topBottom, $lineLength);
    $paddingContent = str_repeat(' ', $paddingLength);
    echo "\n\n".$horizontalLine."\n".$leftRight.$paddingContent.$content.$paddingContent.$leftRight."\n".$horizontalLine."\n";
}