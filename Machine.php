<?php

class Machine implements iCargoItem{
    var $id;
    var $weight;
    var $postDate;
    function __construct(float $weight = 0, \DateTime $postDate = null){
        $this->weight = $weight ? : 500*rand(3,4);
        $this->postDate = $postDate ? : new \DateTime();
        $rand = rand(0,999);
        $this->id = "M".substr(md5($this->weight.$rand), 0, 3).":$rand";
    }

    function getId(): string {
        return $this->id;
    }

    function getWeight(): float {
        return $this->weight;
    }

    function getPostDate(): \DateTime {
        return $this->postDate;
    }

    function __toString()
    {
        return sprintf("Machine named %s. Weight: %.1f t, posted on %s", $this->id, $this->weight/1000, $this->postDate->format('Y-m-d, H:i:s'));
    }
}

