<?php
interface iCargoItem{
    public function getId();
    public function getWeight();
    public function getPostDate();
    public function __toString();
}
